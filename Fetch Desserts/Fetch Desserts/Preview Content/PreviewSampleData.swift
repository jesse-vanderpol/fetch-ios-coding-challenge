//
//  PreviewSampleData.swift
//  Fetch Desserts
//
//  Created by Jesse VanDerPol on 6/22/24.
//

import SwiftUI

actor PreviewSampleData {

}

extension Meal {
    static var recipe1: Meal {
        .init(
            id: "52893",
            name: "Apple & Blackberry Crumble",
            directions: "Some small directions",
            thumbnail: "https://www.themealdb.com/images/media/meals/xvsurr1511719182.jpg",
            ingredients: [Ingredient(name: "First Ingredient", amount: "One", id: 1)]
        )
    }
    static var recipe2: Meal {
        .init(
            id: "53049",
            name: "Apam balik",
            directions: "Mix milk, oil and egg together. Sift flour, baking powder and salt into the mixture. Stir well until all ingredients are combined evenly.\r\n\r\nSpread some batter onto the pan. Spread a thin layer of batter to the side of the pan. Cover the pan for 30-60 seconds until small air bubbles appear.\r\n\r\nAdd butter, cream corn, crushed peanuts and sugar onto the pancake. Fold the pancake into half once the bottom surface is browned.\r\n\r\nCut into wedges and best eaten when it is warm.",
            thumbnail: "https://www.themealdb.com/images/media/meals/adxcbq1619787919.jpg",
            ingredients: [
                Ingredient(name:"Milk", amount: "200ml", id: 1),
                Ingredient(name:"Oil", amount: "60ml", id: 2),
                Ingredient(name:"Eggs", amount: "2", id: 3),
                Ingredient(name:"Flour", amount: "1600g", id: 4),
                Ingredient(name:"Baking Powder", amount: "3 tsp", id: 5),
                Ingredient(name:"Salt", amount: "1/2 tsp", id: 6),
                Ingredient(name:"Unsalted Butter", amount: "25g", id: 7),
                Ingredient(name:"Sugar", amount: "45g", id: 8),
                Ingredient(name:"Peanut Butter", amount: "3 tbs", id: 9),
            ]
        )
    }
}
