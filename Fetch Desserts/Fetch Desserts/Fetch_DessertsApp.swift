//
//  Fetch_DessertsApp.swift
//  Fetch Desserts
//
//  Created by Jesse VanDerPol on 6/19/24.
//

import SwiftUI

@main
struct Fetch_DessertsApp: App {
    
    
    var body: some Scene {
        WindowGroup {
            MealListView()
        }
    }
}
