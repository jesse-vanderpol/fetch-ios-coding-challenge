//
//  ListViewModel.swift
//  Fetch Desserts
//
//  Created by Jesse VanDerPol on 6/23/24.
//

import Foundation

@Observable
final class MealViewModel {
    var BASE_URL = URL(string: "https://themealdb.com/api/json/v1/1/")
    var meals: [Meal] = []
    
    @MainActor
    func filterMealList() async throws {
        let url = URL(string: "filter.php?c=Dessert", relativeTo: BASE_URL)!
        
        let (data, _) = try await URLSession.shared.data(from: url)
        let jsonDecoder = JSONDecoder()
        let result = try jsonDecoder.decode(MealRequest.self, from: data).meals

        meals = MealViewModel.sortMealsByNameAlpha(mealList: result)
    }

    @MainActor
    func lookupMealDetail(withID id: Int) async throws -> Meal? {
        let url = URL(string: "lookup.php?i=\(id)", relativeTo: BASE_URL)!
        
        let (data, _) = try await URLSession.shared.data(from: url)
        let jsonDecoder = JSONDecoder()
        return try jsonDecoder.decode(MealRequest.self, from: data).meals.first
    }
    
    static func sortMealsByNameAlpha(mealList toSort: [Meal]) -> [Meal] {
        return toSort.sorted { LHS, RHS in
            return LHS.name.localizedCompare(RHS.name) == .orderedAscending
        }
    }
}

enum DownloadError: Error {
    case wrongDataFormat(error: Error)
    case missingData
}
