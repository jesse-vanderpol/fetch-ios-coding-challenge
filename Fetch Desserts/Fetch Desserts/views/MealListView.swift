//
//  MealList.swift
//  Fetch Desserts
//
//  Created by Jesse VanDerPol on 6/22/24.
//

import SwiftUI

struct MealListView: View {
    @State private var model = MealViewModel()
    
    var body: some View {
        NavigationStack {
            List(model.meals) { meal in
                NavigationLink {
                    MealDetailView(meal: meal)
                } label: {
                    MealRowView(thumbnail: meal.thumbnail, name: meal.name, id: meal.id)
                }
                    .listRowSeparator(.hidden)
            }.navigationDestination(for: Meal.self) { meal in
                MealDetailView(meal: meal)
            }
            .overlay {
                if model.meals.isEmpty {
                    ContentUnavailableView("Refresh to load desserts", systemImage: "menucard.fill")
                }
            }
            .navigationTitle("Desserts")
            .listStyle(.plain)
        }.refreshable {
            try? await model.filterMealList()
        }.task {
            try? await model.filterMealList()
        }
    }
}

#Preview {
    NavigationStack {
        MealListView()
    }
}
