//
//  MealRow.swift
//  Fetch Desserts
//
//  Created by Jesse VanDerPol on 6/22/24.
//

import SwiftUI

struct MealRowView: View {
    var thumbnail: String
    var name: String
    var id: String
    
    var body: some View {
        HStack(spacing: 8) {
            AsyncImage(url: URL(string: thumbnail)) { image in
                    image.resizable()
                } placeholder: {
                    ProgressView()
                }
                .frame(width: 50, height: 50)
                .cornerRadius(10)
            VStack(alignment: .leading) {
                Text(name).dynamicTypeSize(.xxLarge)
                Text(id.description).dynamicTypeSize(.xSmall)
            }
        }
        .frame(alignment: .topLeading)
        .padding(.vertical, 8)
    }
}

extension MealRowView {
    init(meal: Meal) {
        name = meal.name
        thumbnail = meal.thumbnail
        id = meal.id
    }
}

#Preview {
    VStack {
        MealRowView(meal: .recipe1)
        MealRowView(meal: .recipe2)
    }
    .padding()
    .frame(minWidth: 300, alignment: .leading)
}
