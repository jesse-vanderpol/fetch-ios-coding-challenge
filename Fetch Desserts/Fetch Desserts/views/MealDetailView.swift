//
//  MealDetail.swift
//  Fetch Desserts
//
//  Created by Jesse VanDerPol on 6/22/24.
//

import SwiftUI

struct MealDetailView: View {
    @State private var model = MealViewModel()
    @State private var id: String
    @State private var name: String
    @State private var directions: String
    @State private var ingredients: [Ingredient]
    
    var body: some View {
        ScrollView([.vertical]) {
            VStack(alignment: .leading, spacing: 8) {
                HStack {
                    Text("Recipe:")
                        .bold()
                        .dynamicTypeSize(.xxLarge)
                    Text(name)
                    Spacer()
                }
                VStack(alignment: .leading) {
                    Text("Ingredients:")
                        .bold()
                        .dynamicTypeSize(.xxLarge)
                    VStack {
                        ForEach(ingredients) { item in
                            HStack {
                                Text(item.name)
                                Spacer()
                                Text(item.amount)
                            }
                        }
                    }.padding(.horizontal, 10)
                }
                VStack(alignment: .leading) {
                    Text("Directions:")
                        .bold()
                        .dynamicTypeSize(.xxLarge)
                    Text(directions)
                        .padding(.horizontal, 10)
                        .multilineTextAlignment(.leading)
                    HStack{Spacer()}
                }
            }
            .padding(.horizontal, 20)
        }
        .defaultScrollAnchor(.top)
        .navigationBarTitleDisplayMode(.inline)
        .overlay {
            if ingredients.count == 0 {
                ProgressView()
            }
        }
        .task {
            guard let meal = try? await model.lookupMealDetail(withID: Int(id) ?? 0) else { return }
            id = meal.id
            name = meal.name
            directions = meal.directions
            ingredients = meal.ingredients
        }
    }
}

extension MealDetailView {
    init(meal: Meal) {
        id = meal.id
        name = meal.name
        directions = meal.directions
        ingredients = meal.ingredients
    }
    init(id: String) {
        self.id = id
        self.name = ""
        self.directions = ""
        self.ingredients = []
    }
}

#Preview {
    MealDetailView(
        id: "52857"
    )
}
