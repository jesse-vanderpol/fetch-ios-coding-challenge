//
//  Ingredient.swift
//  Fetch Desserts
//
//  Created by Jesse VanDerPol on 6/21/24.
//

import Foundation

struct Ingredient {
    let id: Int
    let name: String
    let amount: String
    
    init(name: String, amount: String, id: Int) {
        self.name = name
        self.amount = amount
        self.id = id
    }
}

extension Ingredient: Identifiable {}
extension Ingredient: Hashable {}
