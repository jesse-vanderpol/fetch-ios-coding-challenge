//
//  Item.swift
//  Fetch Desserts
//
//  Created by Jesse VanDerPol on 6/19/24.
//

import Foundation

struct MealRequest: Decodable {
    let meals: [Meal]
}

struct Meal {
    var id: String
    var name: String
    var directions: String
    var thumbnail: String
    var ingredients: [Ingredient]
}

extension Meal: Identifiable {}
extension Meal: Hashable {}

extension Meal: Decodable {
    enum CodingKeys: String, CodingKey {
        case id = "idMeal"
        case name = "strMeal"
        case directions = "strInstructions"
        case thumbnail = "strMealThumb"
        case strIngredient1, strIngredient2, strIngredient3, strIngredient4, strIngredient5, strIngredient6, strIngredient7, strIngredient8, strIngredient9, strIngredient10, strIngredient11, strIngredient12, strIngredient13, strIngredient14, strIngredient15, strIngredient16, strIngredient17, strIngredient18, strIngredient19, strIngredient20
        case strMeasure1, strMeasure2, strMeasure3, strMeasure4, strMeasure5, strMeasure6, strMeasure7, strMeasure8, strMeasure9, strMeasure10, strMeasure11, strMeasure12, strMeasure13, strMeasure14, strMeasure15, strMeasure16, strMeasure17, strMeasure18, strMeasure19, strMeasure20
    }
    
    init(from decoder: any Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.directions = try container.decodeIfPresent(String.self, forKey: .directions) ?? ""
        self.thumbnail = try container.decode(String.self, forKey: .thumbnail)
        let strIngredient1 = try container.decodeIfPresent(String.self, forKey: .strIngredient1)
        let strIngredient2 = try container.decodeIfPresent(String.self, forKey: .strIngredient2)
        let strIngredient3 = try container.decodeIfPresent(String.self, forKey: .strIngredient3)
        let strIngredient4 = try container.decodeIfPresent(String.self, forKey: .strIngredient4)
        let strIngredient5 = try container.decodeIfPresent(String.self, forKey: .strIngredient5)
        let strIngredient6 = try container.decodeIfPresent(String.self, forKey: .strIngredient6)
        let strIngredient7 = try container.decodeIfPresent(String.self, forKey: .strIngredient7)
        let strIngredient8 = try container.decodeIfPresent(String.self, forKey: .strIngredient8)
        let strIngredient9 = try container.decodeIfPresent(String.self, forKey: .strIngredient9)
        let strIngredient10 = try container.decodeIfPresent(String.self, forKey: .strIngredient10)
        let strIngredient11 = try container.decodeIfPresent(String.self, forKey: .strIngredient11)
        let strIngredient12 = try container.decodeIfPresent(String.self, forKey: .strIngredient12)
        let strIngredient13 = try container.decodeIfPresent(String.self, forKey: .strIngredient13)
        let strIngredient14 = try container.decodeIfPresent(String.self, forKey: .strIngredient14)
        let strIngredient15 = try container.decodeIfPresent(String.self, forKey: .strIngredient15)
        let strIngredient16 = try container.decodeIfPresent(String.self, forKey: .strIngredient16)
        let strIngredient17 = try container.decodeIfPresent(String.self, forKey: .strIngredient17)
        let strIngredient18 = try container.decodeIfPresent(String.self, forKey: .strIngredient18)
        let strIngredient19 = try container.decodeIfPresent(String.self, forKey: .strIngredient19)
        let strIngredient20 = try container.decodeIfPresent(String.self, forKey: .strIngredient20)
        let strMeasure1 = try container.decodeIfPresent(String.self, forKey: .strMeasure1)
        let strMeasure2 = try container.decodeIfPresent(String.self, forKey: .strMeasure2)
        let strMeasure3 = try container.decodeIfPresent(String.self, forKey: .strMeasure3)
        let strMeasure4 = try container.decodeIfPresent(String.self, forKey: .strMeasure4)
        let strMeasure5 = try container.decodeIfPresent(String.self, forKey: .strMeasure5)
        let strMeasure6 = try container.decodeIfPresent(String.self, forKey: .strMeasure6)
        let strMeasure7 = try container.decodeIfPresent(String.self, forKey: .strMeasure7)
        let strMeasure8 = try container.decodeIfPresent(String.self, forKey: .strMeasure8)
        let strMeasure9 = try container.decodeIfPresent(String.self, forKey: .strMeasure9)
        let strMeasure10 = try container.decodeIfPresent(String.self, forKey: .strMeasure10)
        let strMeasure11 = try container.decodeIfPresent(String.self, forKey: .strMeasure11)
        let strMeasure12 = try container.decodeIfPresent(String.self, forKey: .strMeasure12)
        let strMeasure13 = try container.decodeIfPresent(String.self, forKey: .strMeasure13)
        let strMeasure14 = try container.decodeIfPresent(String.self, forKey: .strMeasure14)
        let strMeasure15 = try container.decodeIfPresent(String.self, forKey: .strMeasure15)
        let strMeasure16 = try container.decodeIfPresent(String.self, forKey: .strMeasure16)
        let strMeasure17 = try container.decodeIfPresent(String.self, forKey: .strMeasure17)
        let strMeasure18 = try container.decodeIfPresent(String.self, forKey: .strMeasure18)
        let strMeasure19 = try container.decodeIfPresent(String.self, forKey: .strMeasure19)
        let strMeasure20 = try container.decodeIfPresent(String.self, forKey: .strMeasure20)
        let ingredientList: [Ingredient] = [
            Ingredient(name: strIngredient1 ?? "", amount: strMeasure1 ?? "", id: 1 ),
            Ingredient(name: strIngredient2 ?? "", amount: strMeasure2 ?? "", id: 2 ),
            Ingredient(name: strIngredient3 ?? "", amount: strMeasure3 ?? "", id: 3 ),
            Ingredient(name: strIngredient4 ?? "", amount: strMeasure4 ?? "", id: 4 ),
            Ingredient(name: strIngredient5 ?? "", amount: strMeasure5 ?? "", id: 5 ),
            Ingredient(name: strIngredient6 ?? "", amount: strMeasure6 ?? "", id: 6 ),
            Ingredient(name: strIngredient7 ?? "", amount: strMeasure7 ?? "", id: 7 ),
            Ingredient(name: strIngredient8 ?? "", amount: strMeasure8 ?? "", id: 8 ),
            Ingredient(name: strIngredient9 ?? "", amount: strMeasure9 ?? "", id: 9 ),
            Ingredient(name: strIngredient10 ?? "", amount: strMeasure10 ?? "", id: 10),
            Ingredient(name: strIngredient11 ?? "", amount: strMeasure11 ?? "", id: 11),
            Ingredient(name: strIngredient12 ?? "", amount: strMeasure12 ?? "", id: 12),
            Ingredient(name: strIngredient13 ?? "", amount: strMeasure13 ?? "", id: 13),
            Ingredient(name: strIngredient14 ?? "", amount: strMeasure14 ?? "", id: 14),
            Ingredient(name: strIngredient15 ?? "", amount: strMeasure15 ?? "", id: 15),
            Ingredient(name: strIngredient16 ?? "", amount: strMeasure16 ?? "", id: 16),
            Ingredient(name: strIngredient17 ?? "", amount: strMeasure17 ?? "", id: 17),
            Ingredient(name: strIngredient18 ?? "", amount: strMeasure18 ?? "", id: 18),
            Ingredient(name: strIngredient19 ?? "", amount: strMeasure19 ?? "", id: 19),
            Ingredient(name: strIngredient20 ?? "", amount: strMeasure20 ?? "", id: 20)
        ]
        self.ingredients = ingredientList.filter { !$0.name.isEmpty }
    }
}
