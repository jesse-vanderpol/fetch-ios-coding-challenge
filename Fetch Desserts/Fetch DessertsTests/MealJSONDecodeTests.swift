//
//  MealJSONDecodeTests.swift
//  Fetch DessertsTests
//
//  Created by Jesse VanDerPol on 6/25/24.
//

import XCTest
@testable import Fetch_Desserts

final class MealJSONDecodeTests: XCTestCase {
    var jsonFilterData: Data?
    var jsonLookupData: Data?
    
    override func setUpWithError() throws {
        let jsonFilterResponseURL = Bundle.main.url(forResource: "mock_filter", withExtension: "json")
        let jsonLookupResponseURL = Bundle.main.url(forResource: "mock_lookup", withExtension: "json")
        jsonFilterData = try Data(contentsOf: jsonFilterResponseURL!)
        jsonLookupData = try Data(contentsOf: jsonLookupResponseURL!)
    }

    override func tearDownWithError() throws {
        
    }

    func testMealFilterJSONDecode() throws {
        guard let data = jsonFilterData else { return XCTFail("Failed JSON URL and Data setup") }
        let meals = try JSONDecoder().decode(MealRequest.self, from: data).meals
        XCTAssertTrue(meals.count > 1, "Meals list contains more then two recipes")
        XCTAssertNotNil(meals.first?.id, "Meal ID has value")
        XCTAssertNotNil(meals.first?.name, "Meal name has a value")
        XCTAssertNotNil(meals.first?.thumbnail, "Meal thumbnail has value")
    }
    
    func testMealFilterSortedAlphabeticallyByName() async throws {
        guard let data = jsonFilterData else { return XCTFail("Failed JSON URL and Data setup") }
        let meals = try JSONDecoder().decode(MealRequest.self, from: data).meals
        let result = MealViewModel.sortMealsByNameAlpha(mealList: meals)
        
        XCTAssertTrue(result.first!.name.localizedCompare(result.last!.name) == .orderedAscending, "Name property value of first meal is less than or equal to that of last meal.")
    }
    
    func testMealLookupJSONDecode() async throws {
        guard let data = jsonLookupData,
              let meal = try JSONDecoder().decode(MealRequest.self, from: data).meals.first else { return XCTFail("Failed JSON URL and Data setup") }
        XCTAssertNotNil(meal.id, "Meal ID has value")
        XCTAssertNotNil(meal.name, "Meal name has a value")
        XCTAssertNotNil(meal.directions, "Meal directions has value")
        XCTAssertNotNil(meal.thumbnail, "Meal thumbnail has value")
        XCTAssertNotNil(meal.ingredients.first, "Meal has an ingredient with value")
    }
    
    func testMealIngredientRemovedEmptyJSON() async throws {
        guard let data = jsonLookupData,
              let meal = try JSONDecoder().decode(MealRequest.self, from: data).meals.first else { return XCTFail("Failed JSON URL and Data setup") }
        XCTAssertNotNil(meal.ingredients.first, "Meal has first ingredient with value")
        XCTAssertNotNil(meal.ingredients.last, "Meal has last ingredient with value")
    }

}
